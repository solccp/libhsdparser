#ifndef HSDTOKENIZER_H
#define HSDTOKENIZER_H

#include <string>
#include <deque>

namespace HSD
{

    enum Token_ID
    {
        ID_WORD = 1000,
        ID_QUOTE,
        ID_LEFTBRACE,
        ID_RIGHTBRACE,
        ID_ASSIGN_OP,
        ID_EOL,
        ID_CHAR,
        ID_SPACE,
        ID_QUOTESTRING,
        ID_MODIFIER
    };

    struct HSDToken
    {
        Token_ID id;
        std::string value;
    };

    bool tokenizeHSD(const std::string& str, std::deque<HSDToken> &tokens);
}



#endif // HSDTOKENIZER_H
