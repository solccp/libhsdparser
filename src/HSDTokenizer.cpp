#include "HSDTokenizer.h"
#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/lex_lexertl.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/bind.hpp>
#include <boost/ref.hpp>
#include <string>
#include <deque>
#include <stack>
#include <sstream>
#include <vector>
#include <boost/spirit/include/phoenix_stl.hpp>
#define BOOST_SPIRIT_DEBUG

namespace HSD
{
    namespace lex = boost::spirit::lex;


//    struct set_lexer_state
//    {
//        set_lexer_state(char const* state_)
//          : state(state_) {}

//        // This is called by the semantic action handling code during the lexing
//        template <typename Iterator, typename Context>
//        void operator()(Iterator const&, Iterator const&
//          , BOOST_SCOPED_ENUM(boost::spirit::lex::pass_flags)&
//          , std::size_t&, Context& ctx) const
//        {
//            ctx.set_state_name(state.c_str());
//        }

//        std::string state;
//    };

//    struct get_string_impl
//    {
//        template <typename Value>
//        struct result
//        {
//            typedef std::string type;
//        };

//        template <typename Value>
//        std::string operator()(Value const& val) const
//        {
//            // transform the token value (here a variant) into a string
//            // at this point the variant holds a pair of iterators
//            typedef boost::iterator_range<char const*> iterpair_type;
//            iterpair_type const& ip = boost::get<iterpair_type>(val);

//            return std::string(ip.begin(), ip.end());
//        }
//    };

//    boost::phoenix::function<get_string_impl> get_string;

    template <typename Lexer>
    struct HSD_tokens : lex::lexer<Lexer>
    {
        HSD_tokens()
        {
//            using boost::phoenix::push_back;
//            using boost::phoenix::ref;

            string = lex::token_def<>("[^ \\[\\]=\\\"\\t\\n\\{\\}]+", ID_WORD);
            quotedstring = lex::token_def<std::string>("\\\"[^\\\"]*\\\"", ID_QUOTESTRING);
            modifier = lex::token_def<std::string>("\\\[[^\\\[]*\\\]", ID_MODIFIER);
            leftbrace = lex::token_def<>("\\{", ID_LEFTBRACE);
            rightbrace = lex::token_def<>("\\}", ID_RIGHTBRACE);
            op_equal = lex::token_def<>("=", ID_ASSIGN_OP);
            eol = lex::token_def<>("\\n|\\r\\n", ID_EOL);
            space = lex::token_def<>("[ \\t]+", ID_SPACE);
            any = lex::token_def<>(",", ID_CHAR);

            this->self = string
                    | modifier
                    | quotedstring
                    | leftbrace
                    | rightbrace
                    | op_equal
                    | eol
                    | space
                    | any
                    ;
        }


        lex::token_def<std::string> quotedstring, modifier;
        lex::token_def<> string, start_quote, end_quote, leftbrace, rightbrace, op_equal, eol, space, any;
    };
    struct counter
    {
        typedef bool result_type;
        template <typename Token>
        bool operator()(Token const& t, std::deque<HSDToken>& strs) const
        {
            typedef boost::iterator_range<const char*> iterator_range;
            const iterator_range& range = boost::get<iterator_range>(t.value());

            HSDToken token;

            switch (t.id())
            {
            case ID_WORD:
                token.id = ID_WORD;
                token.value = std::string(range.begin(), range.end());
                strs.push_back(token);
                break;
            case ID_LEFTBRACE:
                token.id = ID_LEFTBRACE;
                token.value = std::string(range.begin(), range.end());
                strs.push_back(token);
                break;
            case ID_RIGHTBRACE:
                token.id = ID_RIGHTBRACE;
                token.value = std::string(range.begin(), range.end());
                strs.push_back(token);
                break;
            case ID_ASSIGN_OP:
                token.id = ID_ASSIGN_OP;
                token.value = std::string(range.begin(), range.end());
                strs.push_back(token);
                break;
            case ID_EOL:
                token.id = ID_EOL;
                strs.push_back(token);
                break;
            case ID_QUOTE:
                token.id = ID_QUOTE;
                token.value = std::string(range.begin(), range.end());
                strs.push_back(token);
                break;
            case ID_QUOTESTRING:
                token.id = ID_QUOTESTRING;
                token.value = std::string(range.begin()+1, range.end()-1);
                strs.push_back(token);
                break;
            case ID_CHAR:
                token.id = ID_CHAR;
                token.value = std::string(range.begin(), range.end());
                strs.push_back(token);
                break;
            case ID_MODIFIER:
                token.id = ID_MODIFIER;
                token.value = std::string(range.begin()+1, range.end()-1);
                //ignore for now
                strs.push_back(token);
                break;
            case ID_SPACE:
                break;
            }
            return true;
        }
    };

    void postProcessQuotedString(std::deque<HSDToken> &tokens)
    {
        std::stack<HSDToken> previousTokens;
        std::stringstream stream;
        bool QuoteOpened = false;
        while(!tokens.empty())
        {
            HSDToken nextToken = tokens.front();
            tokens.pop_front();

            if (nextToken.id == HSD::ID_QUOTE)
            {
                if (!QuoteOpened)
                {
                    QuoteOpened = true;
                    stream.str("");
                    stream << "\"";
                }
                else
                {
                    stream << "\"";
                    QuoteOpened = false;
                    HSDToken newToken;
                    newToken.id = HSD::ID_QUOTESTRING;
                    newToken.value = stream.str();
                    previousTokens.push(newToken);
                }
                continue;
            }
            previousTokens.push(nextToken);
        }

        while(previousTokens.empty())
        {
            tokens.push_front(previousTokens.top());
            previousTokens.pop();
        }
    }

    bool tokenizeHSD(const std::string &str, std::deque<HSDToken> &tokens)
    {
        std::deque<HSDToken> l_tokens;

        HSD_tokens<lex::lexertl::lexer<>> HSD_tokens_functor;

        std::string l_str = str;

        const char* first = l_str.c_str();
        const char* last = &first[l_str.size()];
        bool r = lex::tokenize(first, last, HSD_tokens_functor,
             boost::bind(counter(), _1, boost::ref(l_tokens)));


        if (r)
        {
            tokens.clear();
//            postProcessQuotedString(l_tokens);
            tokens = l_tokens;
            return true;
        }
        else
        {
            return false;
        }
    }

}

