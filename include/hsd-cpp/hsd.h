#ifndef HSD_H_62B23520_7C8E_11DE_8A39_0800200C9A66
#define HSD_H_62B23520_7C8E_11DE_8A39_0800200C9A66

#if defined(_MSC_VER) || (defined(__GNUC__) && (__GNUC__ == 3 && __GNUC_MINOR__ >= 4) || (__GNUC__ >= 4)) // GCC supports "pragma once" correctly since 3.4
#pragma once
#endif

#include "hsd-cpp/exceptions.h"


#include "hsd-cpp/node/node.h"
#include "hsd-cpp/node/impl.h"
#include "hsd-cpp/node/convert.h"
#include "hsd-cpp/node/iterator.h"
#include "hsd-cpp/node/detail/impl.h"

#include "hsd-cpp/parser.h"


#endif // HSD_H_62B23520_7C8E_11DE_8A39_0800200C9A66
