#ifndef PARSER_H
#define PARSER_H

#include "hsd-cpp/hsd.h"

namespace HSD
{
    Node parseString(std::string str);
}


#endif // PARSER_H
