#-------------------------------------------------
#
# Project created by QtCreator 2014-03-05T18:41:49
#
#-------------------------------------------------

QT       -= gui

TARGET = HSDParser
TEMPLATE = lib
CONFIG += staticlib c++11

SOURCES += \
    src/binary.cpp \
    src/convert.cpp \
    src/directives.cpp \
    src/exp.cpp \
    src/HSDTokenizer.cpp \
    src/memory.cpp \
    src/node_data.cpp \
    src/null.cpp \
    src/ostream_wrapper.cpp \
    src/regex.cpp \
    src/stream.cpp \
    src/parser.cpp

HEADERS += \
    include/hsd-cpp/anchor.h \
    include/hsd-cpp/binary.h \
    include/hsd-cpp/dll.h \
    include/hsd-cpp/exceptions.h \
    include/hsd-cpp/mark.h \
    include/hsd-cpp/noncopyable.h \
    include/hsd-cpp/null.h \
    include/hsd-cpp/ostream_wrapper.h \
    include/hsd-cpp/traits.h \
    include/hsd-cpp/hsd.h \
    include/hsd-cpp/node/convert.h \
    include/hsd-cpp/node/impl.h \
    include/hsd-cpp/node/iterator.h \
    include/hsd-cpp/node/node.h \
    include/hsd-cpp/node/ptr.h \
    include/hsd-cpp/node/type.h \
    include/hsd-cpp/node/detail/bool_type.h \
    include/hsd-cpp/node/detail/impl.h \
    include/hsd-cpp/node/detail/iterator_fwd.h \
    include/hsd-cpp/node/detail/iterator.h \
    include/hsd-cpp/node/detail/memory.h \
    include/hsd-cpp/node/detail/node_data.h \
    include/hsd-cpp/node/detail/node_iterator.h \
    include/hsd-cpp/node/detail/node_ref.h \
    include/hsd-cpp/node/detail/node.h \
    src/collectionstack.h \
    src/directives.h \
    src/exp.h \
    src/indentation.h \
    src/ptr_stack.h \
    src/ptr_vector.h \
    src/regex.h \
    src/regeximpl.h \
    src/setting.h \
    src/stream.h \
    src/streamcharsource.h \
    src/stringsource.h \
    src/token.h \
    src/HSDTokenizer.h \
    include/hsd-cpp/parser.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}

INCLUDEPATH += include boost
